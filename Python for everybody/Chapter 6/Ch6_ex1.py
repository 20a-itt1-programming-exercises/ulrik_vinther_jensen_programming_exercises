# Exercise 1: Write a while loop that starts at the last character in the string and works its way backwards to the first character in the string,
# printing each letter on a separate line, except backwards.


word = input("Enter a word: ")
index = len(word)
reversedString=[]
while index > 0:
    reversedString += word[index - 1]
    index = index - 1
    print(reversedString)



