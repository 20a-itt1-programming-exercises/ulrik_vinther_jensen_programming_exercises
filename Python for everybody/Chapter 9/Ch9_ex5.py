#Exercise 5: Write a program to read through the mail box data and when you find line that starts with “From”, you will split the line into words using the split function.
#We are interested in who sent the message, which is the second word on the From line.

#From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008

#You will parse the From line and print out the second word for each From line, then you will also count the number of From (not From:) lines and print out a count at the end.
#This is a good sample output with a few lines removed:

dictionary_name = dict()
fname = input("Enter mail file here: ")

try:
    fhand = open(fname)
except FileNotFoundError:
    print("File cannot be opened:", fname)
    exit()

for line in fhand:
    email = line.split()
    if len(email) < 2 or email[0] != 'From':
        continue
    else:
        domain_find = email[1].find("@")
        domain = email[1][domain_find+1:]
        if domain not in dictionary_name:
            dictionary_name[domain] = 1
        else:
            dictionary_name[domain] += 1

print(dictionary_name)