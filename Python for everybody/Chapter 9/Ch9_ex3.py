# Exercise 3:Write a program to read through a mail log, build a histogram using a dictionary to count how many messages
# have come from each email address, and print the dictionary.

dictionary_adress = dict()
fname = input("Enter mail file here: ")

try:
    fhand = open(fname)
except FileNotFoundError:
    print("File cannot be opened:", fname)
    exit()

for line in fhand:
    words = line.split()
    if len(words) < 2 or words[0] != 'From':
        continue
    else:
        if words [1] not in dictionary_adress:
            dictionary_adress[words[1]] = 1
        else:
            dictionary_adress[words[1]] += 1

print(dictionary_adress)