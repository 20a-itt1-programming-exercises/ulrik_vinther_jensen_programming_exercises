#Exercise 4: Add code to the above program to figure out who has the most messages in the file. After all the data has been read and the dictionary has been created,
#look through the dictionary using a maximum loop (see Chapter 5: Maximum and minimum loops) to find who has the most messages and print how many messages the person has.
maximum = 0
maximum_address = ''
dictionary_address = dict()
fname = input("Enter mail file here: ")

try:
    fhand = open(fname)
except FileNotFoundError:
    print("File cannot be opened:", fname)
    exit()

for line in fhand:
    words = line.split()
    if len(words) < 2 or words[0] != 'From':
        continue

    if words [1] not in dictionary_address:
        dictionary_address[words[1]] = 1
    else:
        dictionary_address[words[1]] += 1

for address in dictionary_address:
    if dictionary_address[address] > maximum:
        maximum = dictionary_address[address]
        maximum_address = address

print (f"The email address: '{maximum_address}' has the heigest count of: {maximum}")