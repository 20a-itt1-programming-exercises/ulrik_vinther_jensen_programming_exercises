# Exercise 7: Rewrite the grade program from the previous chapter using a function called computegrade that takes a score as its parameter and returns a grade as a string.


def computegrade(score):

    try:

        if score > 1.0:
            print("Error, you can only type in a score from 0.0 to 1.0") 

        elif score >= 0.9 and 1.0 >= score:
            print ("You have recived a grade of A, congratulations")

        elif score >= 0.8 and 1.0 >= score:
            print ("You have recived a grade of B, ouch, so close!")

        elif score >=0.7 and 1.0 >= score:
            print ("You have recived a grade of C, you are getting closer")

        elif score >=0.6 and 1.0 >= score:
            print ("You have recived a grade of D, try again")

        elif score < 0.6 and 1.0 >= score:
            print ("You have recived a grade of F, better luck next time!" )


    except:
        print('Please try again with a numeric input')


