#Exercise 5: What will the following Python program print out?

def fred():
   print("Zap")

def jane():
   print("ABC")

jane()
fred()
jane()

#It will print out:

#ABC
#ZAP
#ABC
