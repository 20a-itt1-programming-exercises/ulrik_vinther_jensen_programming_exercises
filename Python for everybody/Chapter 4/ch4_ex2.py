#Exercise 2: Move the last line of this program to the top, so the function call appears before the definitions. Run the program and see what error message you get.

repeat_lyrics() #Line that has been delete and moved to top instead
def print_lyrics():
    print("I'm a lumberjack, and I'm okay.")
    print('I sleep all night and I work all day.')


def repeat_lyrics():
    print_lyrics()
    print_lyrics()

#Error displayed:
    #Traceback (most recent call last):
    #File "C:\Users\Ulrik\AppData\Roaming\SPB_Data\.ssh\ulrik_vinther_jensen_programming_exercises\Chapter 4\ch4_ex2.py", line 3, in <module>
    #repeat_lyrics() #Line that has been delete and moved to top instead
    #NameError: name 'repeat_lyrics' is not defined

