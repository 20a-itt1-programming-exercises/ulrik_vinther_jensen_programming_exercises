import string
fname = input("Enter mail file here: ")

try:
    fhand = open(fname)

except FileNotFoundError:
    print("File cannot be opened:", fname)
    exit()

counts = dict()
for line in fhand:
    line = line.translate(str.maketrans('', '', string.digits))
    line = line.translate(str.maketrans('', '', string.punctuation))
    line = line.lower()
    words = line.split()
    for word in words:
        for letter in word:
         if letter not in counts:
              counts[letter] = 1
         else:
              counts[letter] += 1

lst = list()
for key, val in list(counts.items()):
    lst.append((val, key))

lst.sort(reverse=True)

for key, val in lst:
    print(key, val)