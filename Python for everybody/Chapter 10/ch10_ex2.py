dictionary_adress = dict()
fname = input("Enter mail file here: ")
_list = list()

try:
    fhand = open(fname)
except FileNotFoundError:
    print("File cannot be opened:", fname)
    exit()

for line in fhand:
    words = line.split()
    if len(words) < 2 or words[0] != 'From':
        continue
    else:
        colon_position = words[5].find(':')
        hour = words[5][:colon_position]
    if hour not in dictionary_adress:
        dictionary_adress[hour] = 1
    else:
        dictionary_adress[hour] += 1

for key, val in list(dictionary_adress.items()):
    _list.append((key, val))

_list.sort()

for key, val in _list:
    print(key, val)

