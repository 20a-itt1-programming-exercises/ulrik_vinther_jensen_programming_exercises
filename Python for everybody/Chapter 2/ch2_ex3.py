#Exercise 3: Write a program to prompt the user for hours and rate per hour to compute gross pay.

#Enter Hours: 35
#Enter Rate: 2.75
#Pay: 96.25

hourly_rate = 2.75
hours = int(input('How many hours did you work? '))
print('You will recieve the following payment:')
print(str(hours * hourly_rate) + ' Dollars')
