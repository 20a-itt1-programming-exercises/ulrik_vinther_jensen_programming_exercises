#Exercise 4: Assume that we execute the following assignment statements:

#width = 17
#height = 12.0

#For each of the following expressions, write the value of the expression and the type (of the value of the expression).

#   a) width//2

#   b) width/2.0

#   c) height/3

#   d) 1 + 2 * 5

#Use the Python interpreter to check your answers.

#I get the following values and type from my answers:

#a)
#a value of 8
# a type of <class 'int'>

#b)
#a value of 8.5
#a type of <class 'float'>

#c)
#a value of 4.0
#a type of <class 'float'>

#d)
#a value of 11
#a type of <class 'int'>







