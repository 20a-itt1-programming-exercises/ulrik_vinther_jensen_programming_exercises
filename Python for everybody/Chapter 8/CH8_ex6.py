#Rewrite the program that prompts the user for a list of numbers and prints out the maximum and minimum of the numbers at the end
#when the user enters “done”. Write the program to store the numbers the user enters in a list and use the max() and min() functions
#to compute the maximum and minimum numbers after the loop completes.

all_nums = []

while True:
    str_val = input('Enter number: ')
    if str_val == 'done':
        break
    try:
        numb = float(str_val)
    except:
        print("Please Enter a number")
        continue
    all_nums.append(numb)

print('Min:',min(all_nums),'Max:' ,max(all_nums))