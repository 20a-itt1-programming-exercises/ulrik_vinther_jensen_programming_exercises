#Exercise 3: Rewrite the guardian code in the above example from exercise 2 without two if statements.
#Instead, use a compound logical expression using the or logical operator with a single if statement.

count = 0

fileopen = input("Enter file.txt to open: ")
fhand = open(fileopen)
for line in fhand:
        words = line.split()
        #print('Debug:', words)
        if len(words) == 0 or  words[0] != 'From':
             continue
        print(words[2])
