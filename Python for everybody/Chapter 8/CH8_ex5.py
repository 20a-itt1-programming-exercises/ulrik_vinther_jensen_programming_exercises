#Write a program to read through the mail box data and when you find line that starts with “From”, you will split the line into words
#using the split function. We are interested in who sent the message, which is the second word on the From line.

file_name = input ("Enter file name: ")
fhand = open(file_name)
count = 0

for line in fhand:
    if line.startswith('From '):
        count = count + 1
        split = line.split()
        print(split)
    else:
        continue
print(f"Total count of email answers are: {count}")