#Write a function called chop that takes a list and modifies it, removing the first and last elements, and returns None.
#Then write a function called middle that takes a list and returns a new list that contains all but the first and last elements.

def chop(elements):
    del elements[0]
    del elements[-1]
    return None

def middle(elements):
    new_elements = elements[1:-1]
    print(new_elements)
