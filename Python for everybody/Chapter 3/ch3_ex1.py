#Exercise 1: Rewrite your pay computation to give the employee 1.5 times the hourly rate for hours worked above 40 hours.

#Enter Hours: 45
#Enter Rate: 10
#Pay: 475.0


bonus_rate = 1.5
hours = int(input('How many hours did you work? '))
hourly_rate = int(input('What is your hourly rate? '))
print('You will recieve the following payment: ')

if hours > 40:  
    overtime = (hours-40)
    overtime_bonus = overtime * hourly_rate * bonus_rate
    payment = (hours - overtime) * hourly_rate + overtime_bonus
    print(payment, 'Dollars')
   
else:
    print((hours * hourly_rate), ' Dollars')


