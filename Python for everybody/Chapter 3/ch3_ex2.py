#Exercise 2: Rewrite your pay program using try and except so that your program handles non-numeric input gracefully by printing a message and exiting the program. The following shows two executions of the program:

#Enter Hours: 20
#Enter Rate: nine
#Error, please enter numeric input

#Enter Hours: forty
#Error, please enter numeric input


bonus_rate = 1.5

try:
    hours = int(input('How many hours did you work? '))
    hourly_rate = int(input('What is your hourly rate? '))
    print('You will recieve the following payment: ')

    if hours > 40:  
        overtime = (hours-40)
        overtime_bonus = overtime * hourly_rate * bonus_rate
        payment = (hours - overtime) * hourly_rate + overtime_bonus
        print(payment, 'Dollars')
   
    else:
        print((hours * hourly_rate), ' Dollars')
        
        

except:
    print('Error, please enter numeric input')
    
                   

