#Write a program to look for lines of the form:
#New Revision: 39772
#Extract the number from each of the lines using a regular expression and the findall() method.
#Compute the average of the numbers and print out the average as an integer.

#Enter file:mbox.txt
#38549

#Enter file:mbox-short.txt
#39756

import re
new_value = []
fname = input ('Enter file: ')
hand = open(fname, 'r')
for line in hand:
    line = line.rstrip()
    line_find = re.findall('^New.*Revision: ([0-9.]+)', line)
    if len(line_find) > 0:
        #Take string inside of list, then make them a int and add them inside of a new list
        for string in line_find:
            number = int(string)
            new_value.append(number)

print(sum(new_value) / len(new_value))
