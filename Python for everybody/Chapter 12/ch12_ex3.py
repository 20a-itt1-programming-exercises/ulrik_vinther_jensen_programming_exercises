import urllib.request

url = input('Enter url: ')
char_count = b""

try:
    fhand = urllib.request.urlopen(url)
    for line in fhand:
        line.decode().strip()
        char_count += line

except:
    print (f'Url: "{url}" is either improperly formatted or is an non-existent URL, try again.')
    exit()

char_count = char_count.decode()
print('\n' + char_count[:3000])
char_count = char_count.strip('\n')
total = len(char_count)
print(f'"The total count of charaters on this website are: {total}"')