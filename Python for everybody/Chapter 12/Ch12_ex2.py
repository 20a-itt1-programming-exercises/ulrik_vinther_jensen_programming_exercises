#Exercise 2: Change your socket program so that it counts the number of characters it has received and stops displaying any text after it has shown 3000 characters. 
#The program should retrieve the entire document and count the total number of characters and display the count of the number of characters at the end of the document.

import socket

url = input('Enter url: ')

try:
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = url.split("/")[2]
    mysock.connect((host, 80))
    cmd = f'GET {url} HTTP/1.0\r\n\r\n'
    cmd = cmd.encode()
    mysock.send(cmd)

except:
    print (f'Url: "{url}" is either improperly formatted or is an non-existent URL, try again.')
    exit()

char_count = b""
while True:
    data = mysock.recv(512)
    if len(data) < 1:
        break
    char_count += data

char_count = char_count.decode()   
print(char_count[:3000])
total = len(char_count)
print(f'"The total count of charaters on this website including header are: {total}"')

mysock.close()
