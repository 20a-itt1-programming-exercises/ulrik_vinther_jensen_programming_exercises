import random

count = 1
human = 0
computer = 0
random_numb = random.randint(1, 9)

while True:


    try:

        guess = int(input("Guess number between 1-9: "))
        count += 1

        if  guess == random_numb:
            human += 1
            count = 1
            random_numb = random.randint(1, 9)
            if human + computer == 5:
                print("Computer:", computer, "Human:", human, "Byebyee")
                break
            else:
                try_again = input("You win :P:P! try again? (y/n): ")

            if  try_again == 'n':
                print("Computer:", computer, "Human:", human, "Byebyee")
                break
            elif try_again == 'y':
                pass    
        
        elif count < 4:
            print('wrong guess, try again!')

        elif count > 3:
            computer += 1
            count = 1
            random_numb = random.randint(1, 9)
            if computer + human == 5:
                print("Computer:", computer, "Human:", human, "Byebyee")
                break
            else:
                try_again = input("You lose :P:P! try again? (y/n): ")

            if  try_again == 'n':
                print("Computer:", computer, "Human:", human, "Byebyee")
                break
            elif try_again == 'y':
                pass    

    except:
        print('Thats not even a number....')
