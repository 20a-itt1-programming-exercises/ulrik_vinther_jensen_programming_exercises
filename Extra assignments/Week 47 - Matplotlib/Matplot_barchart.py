from matplotlib import pyplot as plt

dictionary_name = dict()
fname = input("Enter mail file here: ")

try:
    fhand = open(fname)
except FileNotFoundError:
    print("File cannot be opened:", fname)
    exit()

for line in fhand:
    email = line.split()
    if len(email) < 2 or email[0] != 'From':
        continue
    else:
        domain_find = email[1].find("@")
        domain = email[1][domain_find+1:]
        if domain not in dictionary_name:
            dictionary_name[domain] = 1
        else:
            dictionary_name[domain] += 1

#Making dict into list
mail_list=list()
count_list=list()
for mail, count in list(dictionary_name.items()):
	mail_list.append(mail)
	count_list.append(count)

#plotting graph
plt.barh(mail_list, count_list)
plt.title('Count from mail')
plt.show()




