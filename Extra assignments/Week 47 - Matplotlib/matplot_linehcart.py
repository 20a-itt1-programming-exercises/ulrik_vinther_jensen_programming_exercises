from matplotlib import pyplot as plt

dictionary_days = dict()

fname = input("Enter mail file here: ")

try:
    fhand = open(fname)
except FileNotFoundError:
    print("File cannot be opened:", fname)
    exit()

for line in fhand:
    words = line.split()
    if len(words) < 3 or words[0] != 'From':
        continue
    else:
        if words[2] not in dictionary_days:
            dictionary_days[words[2]] = 1
        else:
            dictionary_days[words[2]] += 1

#Making dict into list
days_list=list()
count_list=list()
for day, count in list(dictionary_days.items()):
	days_list.append(day)
	count_list.append(count)

#plotting graph
plt.plot(days_list, count_list)
plt.title('Count of days')
plt.show()